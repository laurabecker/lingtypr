#' @title mydata2
#' @docType data
#' @usage data(mydata2)
#' @format An object of class \code{"dataframe"}.
#' \describe{
#' \item{glottocode}{glottocode of the language}
#' \item{some_feature}{a toy feature}
#' }
#' @keywords datasets
#' @examples data(mydata2)
#' head(mydata2)
"mydata2"
