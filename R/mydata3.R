#' @title mydata3
#' @docType data
#' @usage data(mydata3)
#' @format An object of class \code{"dataframe"}.
#' \describe{
#' \item{wals_code}{wals code of the language}
#' \item{some_feature}{a toy feature}
#' }
#' @keywords datasets
#' @examples data(mydata3)
#' head(mydata3)
"mydata3"
